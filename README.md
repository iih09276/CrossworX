# CrossworX

CrossworX is an application to help solve simple to complex word bank crossword puzzles

## Installation

Download the program and run index.html
## Usage

-Solver: uses the number of letters to filter out any possible words.


-Refine: Uses selected text box to further isolate and filter out words. (And search words to find out it's meaning.)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GPLv2](https://gitlab.com/iih09276/CrossworX/-/raw/main/LICENSE/)
